package de.humanfork.experiment.jgit;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Implemented by {@link GitCloneParameterResolver}. */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface GitClone {

    public String repoUri();
        
    public String usernameParameter() default UsernamePasswordSystemParameterCredentialProvider.DEFAULT_USERNAME_PARAMTER;
    public String passwordParameter() default UsernamePasswordSystemParameterCredentialProvider.DEFAULT_PASSWORD_PARAMTER;;
    
}
