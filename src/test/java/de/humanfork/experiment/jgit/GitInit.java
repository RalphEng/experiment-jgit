package de.humanfork.experiment.jgit;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** Implemented by {@link GitCloneParameterResolver}. */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface GitInit {

    public String name() default "";
    
    public String sourceContentDir() default "";
        
}
