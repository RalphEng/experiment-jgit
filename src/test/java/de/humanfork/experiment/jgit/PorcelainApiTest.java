package de.humanfork.experiment.jgit;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.DepthWalk.RevWalk;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Logger;
import com.jcraft.jsch.Session;

/**
 * JGit has two basic levels of API: plumbing and porcelain. - porcelain APIs –
 * front-end for common user-level actions (similar to Git command-line tool) -
 * plumbing APIs – direct interacting with low-level repository objects
 *
 * @author engelmann
 */
@ExtendWith(GitCloneParameterResolver.class)
public class PorcelainApiTest {

    private static final String GIT_EXPERIMENT_URI = "https://gitlab.com/RalphEng/experiment-jgit.git";

    @Test
    public void testCreate() throws GitAPIException {
        Path localRepoLocation = GitTestUtil.createTempDir("testCreate");
        Git git = createNewRepository(localRepoLocation);

        printDirectorContent(localRepoLocation);
    }

    /**
     * Requires System Parameter: -DUsername and -DPassword
     *
     * @throws GitAPIException
     */
    @UsernamePasswordRequried
    @Test
    public void testCloneHttps() throws GitAPIException {
        Path localRepoLocation = GitTestUtil.createTempDir("testCloneHttps");
        Git git = cloneRepositoryViaHttps(localRepoLocation, URI.create(GIT_EXPERIMENT_URI));

        printDirectorContent(localRepoLocation);
    }

    /**
     * Conntect to gitlab via SSH.
     * Your private RSA key is required (for example c:/Users/Ralph/.ssh/ralphRshcDe_GitLab_rsa).
     * Use System property {@code gitIdentiyRsaPrivateKeyFile} to specifiy the RSA file.
     */
    @Test
    @SystemParameterRequried("gitIdentiyRsaPrivateKeyFile")
    public void testCloneSsh() throws GitAPIException {

        Path gitIdentiyRsaPrivateKeyFile = Path.of(getSystemProperty("gitIdentiyRsaPrivateKeyFile"));

        enableJschLogging();
        Path localRepoLocation = GitTestUtil.createTempDir("testClone");
        Git git = cloneRepositoryViaSsh(localRepoLocation,
                URI.create("ssh://git@gitlab.com:RalphEng/experiment-jgit.git"),
                gitIdentiyRsaPrivateKeyFile);

        printDirectorContent(localRepoLocation);
    }

    /**
     * Requires System Parameter: -DgitlabExperimentUnitTestToken
     * This token could be a Gitlab "Deploy Token" for project "experiment-jgit"
     * (Deploy tokens allow read-only access to your repository and registry images.
     *  They can been created via: project/settings/repository/Deploy Tokens)
     */
    private void enableJschLogging() {
        JSch.setLogger(new Logger() {

            @Override
            public void log(final int level, final String message) {
                System.out.println(level + " \t" + message);
            }

            @Override
            public boolean isEnabled(final int level) {
                return true;
            }
        });
    }

    /*
     * Requires System Parameter: -DGitlabAccessToken
     */
    @SystemParameterRequried("gitlabExperimentUnitTestToken")
    @Test
    public void testCloneGitlabAccessToken() throws GitAPIException {

        Path localRepoLocation = GitTestUtil.createTempDir("testCloneToken");
        String gitlabAccessToken = getSystemProperty("gitlabExperimentUnitTestToken");

        URI uri = URI
                .create("https://gitlab-ci-token:" + gitlabAccessToken + "@gitlab.com/RalphEng/experiment-jgit.git");

        CloneCommand cloneCommand = Git.cloneRepository()
                .setDirectory(localRepoLocation.toAbsolutePath().toFile())
                .setURI(uri.toString())
                .setCredentialsProvider(
                        new UsernamePasswordCredentialsProvider("gitlabExperimentUnitTest", gitlabAccessToken));
        Git git = cloneCommand.call();

        printDirectorContent(localRepoLocation);
    }

    private String getSystemProperty(final String name) {
        Objects.requireNonNull(name);

        String property = System.getProperty(name);
        if (property != null) {
            return property;
        } else {
            throw new RuntimeException("System Property `" + name + "` not found");
        }
    }

    @Test
    public void testGetHead(@GitClone(repoUri = GIT_EXPERIMENT_URI) final Git git)
            throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException {

        Repository repo = git.getRepository();
        ObjectId head = repo.resolve("HEAD");

        System.out.println("head:" + head);
    }

    @Test
    public void testRevWalk(@GitClone(repoUri = GIT_EXPERIMENT_URI) final Git git)
            throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException {

        Repository repo = git.getRepository();
        RevWalk walk = new RevWalk(repo, 100);

        System.out.println("walk:" + walk);
    }

    private void printDirectorContent(final Path directory) {
        Objects.requireNonNull(directory);

        Path absolutePath = directory.toAbsolutePath();
        System.out.println(absolutePath + " : ");
        try {
            Files.list(absolutePath).sorted().map(Path::toString).map(s -> " - " + s).forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException("Exception wile printing directory content of `" + directory + "`");
        }
    }

    private Git createNewRepository(final Path localRepoLocation) throws GitAPIException {
        Objects.requireNonNull(localRepoLocation);

        InitCommand initCommand = Git.init().setDirectory(localRepoLocation.toAbsolutePath().toFile());
        Git git = initCommand.call();

        System.out.println("new git repository created, git: " + git);
        return git;
    }

    private Git cloneRepositoryViaHttps(final Path localRepoLocation, final URI repositoryUri) throws GitAPIException {
        Objects.requireNonNull(localRepoLocation);
        Objects.requireNonNull(repositoryUri);
        argumentRequireScheme("repositoryUri", repositoryUri, "https");

        CloneCommand cloneCommand = Git.cloneRepository()
                .setDirectory(localRepoLocation.toAbsolutePath().toFile())
                .setURI(repositoryUri.toString())
                .setCredentialsProvider(new SystemParameterCredentialProvider());
        Git git = cloneCommand.call();

        System.out.println("clone git repository, git: " + git);
        return git;
    }

    /*
     * https://www.codeaffine.com/2014/12/09/jgit-authentication/
     *
     * https://dzone.com/articles/how-to-authenticate-with-jgit
     *
     * https://github.com/centic9/jgit-cookbook/blob/master/src/main/java/org/
     * dstadler/jgit/porcelain/CloneRemoteRepositoryWithAuthentication.java
     */
    private Git cloneRepositoryViaSsh(final Path localRepoLocation, final URI repositoryUri,
            final Path gitIdentiyRsaPrivateKeyFile)
            throws GitAPIException {
        Objects.requireNonNull(localRepoLocation);
        Objects.requireNonNull(repositoryUri);
        argumentRequireScheme("repositoryUri", repositoryUri, "ssh");

        /*
         * There is a bug in JGIT, if the URI is "ssh://git@gitlab.com:Ralph..." it will become
         * "ssh:///git@gitlab.com:Ralph..." (with three /) and that cause an
         *  org.eclipse.jgit.errors.NotSupportedException: URI not supported: ssh:///git@gitlab.com:RalphEng/experiment-jgit.git
         * The workaround is to use an URI without "ssh://" so just "git@gitlab.com:Ralph..."
         */
        String fixedUriString = repositoryUri.toString().substring("ssh://".length());

        CloneCommand cloneCommand = Git.cloneRepository()
                .setDirectory(localRepoLocation.toAbsolutePath().toFile())
                .setURI(fixedUriString)
                .setTransportConfigCallback(
                        transport -> ((SshTransport) transport)
                                .setSshSessionFactory(new SshSessionFactory(gitIdentiyRsaPrivateKeyFile)));
        Git git = cloneCommand.call();

        System.out.println("clone git repository, git: " + git);
        return git;
    }

    public static class SshSessionFactory extends JschConfigSessionFactory {

        private final Path identiyRsaPrivateKeyFile;

        public SshSessionFactory(final Path identiyRsaPrivateKeyFile) {
            super();
            this.identiyRsaPrivateKeyFile = identiyRsaPrivateKeyFile;
        }

        @Override
        protected void configure(final Host hc, final Session session) {
        }

        @Override
        protected JSch createDefaultJSch(final org.eclipse.jgit.util.FS fs)
                throws JSchException {
            JSch defaultJSch = super.createDefaultJSch(fs);
            defaultJSch.addIdentity(this.identiyRsaPrivateKeyFile.toAbsolutePath().toString());
            return defaultJSch;
        }
    }

    private void argumentRequireScheme(final String argumentName, final URI uri, final String requiredScheme) {
        Objects.requireNonNull(argumentName);
        Objects.requireNonNull(uri);
        Objects.requireNonNull(requiredScheme);

        if (!uri.getScheme().equalsIgnoreCase(requiredScheme)) {
            throw new IllegalArgumentException("uri given argument `" + argumentName + "` must use scheme `"
                    + requiredScheme + "` but is `" + uri.getScheme() + "` (`" + uri + "`)");
        }
    }

}
