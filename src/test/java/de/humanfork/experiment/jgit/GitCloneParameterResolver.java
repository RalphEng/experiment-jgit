package de.humanfork.experiment.jgit;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URI;
import java.nio.file.Path;
import java.util.Objects;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.platform.commons.util.AnnotationUtils;

/**
 * JUnit {@link ParameterResolver} for an parameter of type {@link Git} that is annotated by {@link GitClone}.
 *
 * Like: {@code @Test public void testGetHead(@GitClone(repoUri = GIT_EXPERIMENT_URI) final Git git)...}
 *
 * If one test requires a parameter of type {@link Git} then, this resolver will: clone the
 * {@value GitCloneParameterResolver#DEFAULT_REPO_URI} repository and provide this as {@link Git} parameter.
 *
 * To access the remote directory, credentials are needed, they must been provided by system parameters given
 * with the {@link GitClone} annotation.
 *
 * The clone will been executed in a temp directory, named after the test.
 *
 * @author Ralph Engelmann
 */
public class GitCloneParameterResolver implements ParameterResolver, ExecutionCondition {

    @Override
    public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return supportsParameter(parameterContext.getParameter());
    }

    private boolean supportsParameter(final Parameter parameter) {
        return parameter.getType() == Git.class
                && AnnotationUtils.findAnnotation(parameter, GitClone.class).isPresent();
    }

    @Override
    public Git resolveParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
            throws ParameterResolutionException {

        GitClone gitCloneAnnotation = parameterContext.findAnnotation(GitClone.class).get();

        Path localRepoLocation = GitTestUtil.createTempDir(extensionContext.getDisplayName());
        CredentialsProvider credentialsProvider = buildCredentialProvider(gitCloneAnnotation);

        return cloneRepository(localRepoLocation, URI.create(gitCloneAnnotation.repoUri()), credentialsProvider);
    }

    private UsernamePasswordSystemParameterCredentialProvider buildCredentialProvider(
            final GitClone gitCloneAnnotation) {
        return new UsernamePasswordSystemParameterCredentialProvider(gitCloneAnnotation.usernameParameter(),
                gitCloneAnnotation.passwordParameter());
    }

    private Git cloneRepository(final Path localRepoLocation, final URI repositoryUri,
            final CredentialsProvider credentialsProvider) {
        Objects.requireNonNull(localRepoLocation);
        Objects.requireNonNull(repositoryUri);
        Objects.requireNonNull(credentialsProvider);

        try {
            CloneCommand cloneCommand = Git.cloneRepository()
                    .setDirectory(localRepoLocation.toAbsolutePath().toFile())
                    .setURI(repositoryUri.toString())
                    .setCredentialsProvider(credentialsProvider)
                    .setCloneAllBranches(true);

            Git git = cloneCommand.call();

            System.out.println("clone git repository, git: " + git);
            return git;
        } catch (GitAPIException e) {
            throw new RuntimeException("error while clone repo `" + repositoryUri + "`", e);
        }
    }

    /**
     * Disable the test if it has a {@link Git} parameter, but there is no system parameter for username and password.
     *
     * @param context the context
     * @return enable all tests except if it has a {@link Git} parameter and no system parameter for username and password.
     */
    @Override
    public ConditionEvaluationResult evaluateExecutionCondition(final ExtensionContext context) {

        if (context.getTestMethod().isPresent()) {
            Method method = context.getTestMethod().get();

            for (Parameter parameter : method.getParameters()) {
                if (this.supportsParameter(parameter)) {
                    UsernamePasswordSystemParameterCredentialProvider credentialProvider = buildCredentialProvider(
                            AnnotationUtils.findAnnotation(parameter, GitClone.class).get());
                    if (!credentialProvider.checkSystemParameterDefined()) {
                        return ConditionEvaluationResult
                                .disabled(String.format("System Properties %s and %s not found but required",
                                        credentialProvider.getUsernameParameter(),
                                        credentialProvider.getPasswordParameter()));
                    }
                }
            }
            return ConditionEvaluationResult.enabled("");
        } else {
            return ConditionEvaluationResult.enabled("");
        }
    }
}
