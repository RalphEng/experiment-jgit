package de.humanfork.experiment.jgit;

import org.junit.platform.commons.util.AnnotationUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Junit5 {@link ExecutionCondition} implementation for {@link SystemParameterRequried}.
 * @author engelmann
 */
public class SytemParameterRequiredConditon implements ExecutionCondition {

    @Override
    public ConditionEvaluationResult evaluateExecutionCondition(final ExtensionContext context) {
        Objects.requireNonNull(context);

        Optional<SystemParameterRequried> annotation = AnnotationUtils.findAnnotation(context.getElement(),
                SystemParameterRequried.class);

        return annotation.map(spr -> this.checkSystemProperties(spr.value()))
                .orElseGet(() -> ConditionEvaluationResult.enabled("because no constraint"));
    }

    private ConditionEvaluationResult checkSystemProperties(String propertieNames) {
        List<String> notSetProperties = Pattern.compile(",").splitAsStream(propertieNames)
                .filter(Predicate.not(this::isSystemPropertySet)).collect(Collectors.toList());

        if (notSetProperties.isEmpty()) {
            return ConditionEvaluationResult.enabled("all properites set");
        } else {
            return ConditionEvaluationResult.disabled("not set properites: " + String.join(", ", notSetProperties));
        }
    }

    private boolean isSystemPropertySet(String propertyName) {
        return System.getProperty(propertyName.trim()) != null;
    }

}
