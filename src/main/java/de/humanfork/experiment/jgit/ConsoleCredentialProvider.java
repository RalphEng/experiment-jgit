package de.humanfork.experiment.jgit;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.CredentialItem.CharArrayType;
import org.eclipse.jgit.transport.CredentialItem.InformationalMessage;
import org.eclipse.jgit.transport.CredentialItem.StringType;
import org.eclipse.jgit.transport.CredentialItem.YesNoType;

/**
 * Ask the user via console for credentials.
 */
public class ConsoleCredentialProvider extends CredentialsProvider {

    @Override
    public boolean supports(CredentialItem... items) {
        return Arrays.stream(items).anyMatch(this::support);
    }

    public boolean support(CredentialItem item) {
        return (item instanceof StringType
                || item instanceof CharArrayType
                || item instanceof YesNoType
                || item instanceof InformationalMessage);
    }

    @Override
    public boolean isInteractive() {
        return true;
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
        System.out.println("uri: `" + uri + "`");
        System.out.println("items: " + Arrays.stream(items).map(CredentialItem::toString)
                .collect(Collectors.joining(", ", "[", "]")));

        for (CredentialItem item : items) {
            if (System.console() == null) {
                throw new RuntimeException(
                        "could not ask `" + item.getPromptText() + "` because no console available ");
            }

            System.console().printf(item.getPromptText());

            if (item instanceof StringType) {
                ((StringType) item).setValue(System.console().readLine());
            } else if (item instanceof CharArrayType) {
                ((CharArrayType) item).setValue(System.console().readLine().toCharArray());
            } else if (item instanceof YesNoType) {
                ((YesNoType) item).setValue(Boolean.parseBoolean(System.console().readLine()));
            } else if (item instanceof InformationalMessage) {
                // noting to do;
            } else {
                throw new RuntimeException("unsupported CredentialItem:  " + item);
            }
        }

        return true;
    }
}
