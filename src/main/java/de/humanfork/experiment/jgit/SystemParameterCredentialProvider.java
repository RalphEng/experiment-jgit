package de.humanfork.experiment.jgit;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.CredentialItem.CharArrayType;
import org.eclipse.jgit.transport.CredentialItem.InformationalMessage;
import org.eclipse.jgit.transport.CredentialItem.StringType;
import org.eclipse.jgit.transport.CredentialItem.YesNoType;

/**
 * Read credentials form System parameters.
 * 
 * This provides just try to read a system parameter for each requried {@link CredentialItem}, by its promt text:
 * {@code System.getProperty(credentialItem..getPromptText()replaceAll("\\s", ""))}.
 */
public class SystemParameterCredentialProvider extends CredentialsProvider {

    @Override
    public boolean supports(CredentialItem... items) {
        return Arrays.stream(items).anyMatch(this::support);
    }

    public boolean support(CredentialItem item) {
        return (item instanceof StringType
                || item instanceof CharArrayType
                || item instanceof YesNoType
                || item instanceof InformationalMessage);
    }

    @Override
    public boolean isInteractive() {
        return true;
    }

    @Override
    public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
        System.out.println("uri: `" + uri + "`");
        System.out.println("items: " + Arrays.stream(items).map(CredentialItem::toString)
                .collect(Collectors.joining(", ", "[", "]")));

        for (CredentialItem item : items) {
            if (item instanceof StringType) {
                ((StringType) item).setValue(getSystemProperty(item.getPromptText()));
            } else if (item instanceof CharArrayType) {
                ((CharArrayType) item).setValue(getSystemProperty(item.getPromptText()).toCharArray());
            } else if (item instanceof YesNoType) {
                ((YesNoType) item).setValue(Boolean.parseBoolean(getSystemProperty(item.getPromptText())));
            } else if (item instanceof InformationalMessage) {
                // noting to do;
            } else {
                throw new RuntimeException("unsupported CredentialItem:  " + item);
            }
        }

        return true;
    }

    private String getSystemProperty(String message) {
        String key = message.replaceAll("\\s", "");
        String value = System.getProperty(key);
        System.out.println("message: " + message + ", key=`" + key + "`, value=`" + value + "`");

        if (value == null) {
            throw new RuntimeException("System Property `" + key + "` is missing but required");
        } else {
            return value;
        }
    }
}
