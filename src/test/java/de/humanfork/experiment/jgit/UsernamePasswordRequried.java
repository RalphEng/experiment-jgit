package de.humanfork.experiment.jgit;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * Disable the test if the System parameter "Username" and "Password" are not provided.
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@SystemParameterRequried("Username, Password")
public @interface UsernamePasswordRequried {

}
