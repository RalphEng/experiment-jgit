package de.humanfork.experiment.jgit;

import org.junit.jupiter.api.extension.ExtendWith;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;


/**
 * The annotated test will been disabled if at least on of the named ({@code value}) system parameter is not present.
 * 
 * This annotation is able to handle multiple system parameters (comma seperated).
 * 
 * @author engelmann
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(SytemParameterRequiredConditon.class)
public @interface SystemParameterRequried {

    /**
     * Comma separated system property names.
     */
    public String value();
}
