Experiment: JGit
================


Some experiments with JGit https://www.eclipse.org/jgit/

```
<dependency>
    <groupId>org.eclipse.jgit</groupId>
    <artifactId>org.eclipse.jgit</artifactId>   
</dependency>
```

> _**NOTE: JUnit5 case study**_
>
> _This experiment is also a case study for JUnit5 extensions, like `ParameterResolver` and `ExecutionCondition`._
>
> _See:_
> * _`SytemParameterRequiredConditon` and`UsernamePasswordRequried`, (`ExecutionCondition`)_
> * _`GitCloneParameterResolver`, or `GitInitParameterResolver` (ParameterResolver)_
> _used in `PorcelainApiTest`_
> ```
> @ExtendWith(GitCloneParameterResolver.class)
> public class PorcelainApiTest {
>    @Test
>    @SystemParameterRequried("gitIdentiyRsaPrivateKeyFile")
>    public void testCloneSsh() throws GitAPIException {...}
>
>    ...
>    @Test
>    public void testGetHead(@GitClone(repoUri = GIT_EXPERIMENT_URI) final Git git) {...}
>
> }
> ```


## Usefull links

- [JGit Authentication Explained](https://www.codeaffine.com/2014/12/09/jgit-authentication/)
