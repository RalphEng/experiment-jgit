package de.humanfork.experiment.jgit;

import java.util.Arrays;
import java.util.Optional;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;

/**
 * Credential provider that provides username and password from (fix) system parameters.
 */
public class UsernamePasswordSystemParameterCredentialProvider extends CredentialsProvider {

    /** Default name of the System parameter to obtain the username: {@value #DEFAULT_USERNAME_PARAMTER}. */
    public static final String DEFAULT_USERNAME_PARAMTER = "Username";

    /** Default name of the System parameter to obtain the password: {@value #DEFAULT_PASSWORD_PARAMTER}. */
    public static final String DEFAULT_PASSWORD_PARAMTER = "Password";

    /** The name of the System parameter to obtain the username. */
    private final String usernameParameter;

    /** The name of the System parameter to obtain the password. */
    private final String passwordParameter;

    public UsernamePasswordSystemParameterCredentialProvider(final String usernameParameter,
            final String passwordParameter) {
        this.usernameParameter = usernameParameter;
        this.passwordParameter = passwordParameter;
    }

    /**
     * Instantiate a {@link UsernamePasswordSystemParameterCredentialProvider} with default username- and password-
     * parameter names.
     * @see #DEFAULT_USERNAME_PARAMTER
     * @see #DEFAULT_PASSWORD_PARAMTER
     */
    public UsernamePasswordSystemParameterCredentialProvider() {
        this(DEFAULT_USERNAME_PARAMTER, DEFAULT_USERNAME_PARAMTER);
    }

    public String getUsernameParameter() {
        return usernameParameter;
    }

    public String getPasswordParameter() {
        return passwordParameter;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isInteractive() {
        return false;
    }

    @Override
    public boolean supports(final CredentialItem... items) {
        return Arrays.stream(items).anyMatch(this::support);
    }

    public boolean support(final CredentialItem item) {
        return (item instanceof CredentialItem.Username || item instanceof CredentialItem.Password);
    }

    /** {@inheritDoc} */
    @Override
    public boolean get(final URIish uri, final CredentialItem... items)
            throws UnsupportedCredentialItem {
        for (CredentialItem i : items) {
            if (i instanceof CredentialItem.Username) {
                ((CredentialItem.Username) i).setValue(getSystemProperty(usernameParameter));
            } else if (i instanceof CredentialItem.Password) {
                ((CredentialItem.Password) i).setValue(getSystemProperty(passwordParameter).toCharArray());
            } else if (i instanceof CredentialItem.StringType && i.getPromptText().equals("Password: ")) {
                ((CredentialItem.StringType) i).setValue(getSystemProperty(passwordParameter));
            } else {
                throw new UnsupportedCredentialItem(uri, i.getClass().getName() + ":" + i.getPromptText());
            }
        }
        return true;
    }

    private String getSystemProperty(final String parameterName) {
        return findSystemProperty(parameterName).orElseThrow(
                () -> new RuntimeException("System Property `" + parameterName + "` is missing but required"));
    }

    private Optional<String> findSystemProperty(final String parameterName) {
        return Optional.ofNullable(System.getProperty(parameterName));
    }

    /**
     * Check that the system parameters for username and password are defined.
     */
    public boolean checkSystemParameterDefined() {
        return findSystemProperty(usernameParameter).isPresent()
                && findSystemProperty(passwordParameter).isPresent();
    }

}
