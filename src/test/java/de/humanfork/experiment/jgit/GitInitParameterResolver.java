package de.humanfork.experiment.jgit;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.platform.commons.util.AnnotationUtils;

/**
 * JUnit {@link ParameterResolver} for an parameter of type {@link Git} that is annotated by {@link GitInit}.
 *
 * If one test requires a parameter of type {@link Git} then, this resolver will: clone the
 * {@value GitInitParameterResolver#DEFAULT_REPO_URI} repository and provide this as {@link Git} parameter.
 *
 * To access the remote directory, credentials are needed, they must been provided by system parameters given
 * with the {@link GitClone} annotation.
 *
 * The clone will been executed in a temp directory, named after the test.
 *
 * @author Ralph Engelmann
 */
public class GitInitParameterResolver implements ParameterResolver {

    @Override
    public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return supportsParameter(parameterContext.getParameter());
    }

    private boolean supportsParameter(final Parameter parameter) {
        return parameter.getType() == Git.class
                && AnnotationUtils.findAnnotation(parameter, GitInit.class).isPresent();
    }

    @Override
    public Git resolveParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
            throws ParameterResolutionException {

        GitInit gitCloneAnnotation = parameterContext.findAnnotation(GitInit.class).get();
        String name = (!gitCloneAnnotation.name().isBlank() ? gitCloneAnnotation.name() : extensionContext.getDisplayName());
        String sourceContentDir = gitCloneAnnotation.sourceContentDir();
        
        Path localRepoLocation = GitTestUtil.createTempDir(name);
        Git git = initRepository(localRepoLocation);
        
        if (!sourceContentDir.isBlank()) {
            copy(sourceContentDir, git.getRepository().getWorkTree());
        }
        
        return git;
    }

    private void copy(String sourceContentDir, File workTree) {
        // TODO Auto-generated method stub
        
    }

    public static Path createTempDir(final String title) {
        try {
            Path path = Files.createTempDirectory("jgit-experiment-" + title);
            System.out.println("temp dir: " + path.toAbsolutePath());
            return path;
        } catch (IOException e) {
            throw new RuntimeException("could not create temp directory", e);
        }
    }

    private Git initRepository(final Path localRepoLocation) {
        Objects.requireNonNull(localRepoLocation);
        try {
            InitCommand initCommand = Git.init().setDirectory(localRepoLocation.toAbsolutePath().toFile());
            Git git = initCommand.call();

            System.out.println("new git repository created, git: " + git);
            return git;
        } catch (GitAPIException e) {
            throw new RuntimeException("error while init repo `" + localRepoLocation + "`", e);
        }
    }

    private Git cloneRepository(final Path localRepoLocation, final URI repositoryUri,
            final CredentialsProvider credentialsProvider) {
        Objects.requireNonNull(localRepoLocation);
        Objects.requireNonNull(repositoryUri);
        Objects.requireNonNull(credentialsProvider);

        try {
            CloneCommand cloneCommand = Git.cloneRepository()
                    .setDirectory(localRepoLocation.toAbsolutePath().toFile())
                    .setURI(repositoryUri.toString())
                    .setCredentialsProvider(credentialsProvider)
                    .setCloneAllBranches(true);

            Git git = cloneCommand.call();

            System.out.println("clone git repository, git: " + git);
            return git;
        } catch (GitAPIException e) {
            throw new RuntimeException("error while clone repo `" + repositoryUri + "`", e);
        }
    }

}
