package de.humanfork.experiment.jgit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class GitTestUtil {

    public static Path createTempDir(final String title) {
        try {
            Path path = Files.createTempDirectory("jgit-experiment-" + title);
            System.out.println("temp dir: " + path.toAbsolutePath());
            return path;
        } catch (IOException e) {
            throw new RuntimeException("could not create temp directory", e);
        }
    }
}
